# To use with thin
# thin start -p PORT -R config.ru

# Require sinatra/base instead of sinatra to avoid some of the magic
# This keeps us from polluting the top level namespace with sinatra's
# magic methods.
require 'sinatra/base'
require 'sinatra/cookies'

# Rack add-ons
require 'rack'
require 'rack/rewrite'
require 'rack/cache'
require 'rack/protection'
require 'rack-livereload'
require 'rack-flash'

# Application utils
require 'yaml'
require 'mail'
require 'xml-sitemap'

# Twitter API
require 'twitter'

# Rendering
require 'kramdown'
require 'mustache'

# @Application
module Application
  # @Volta
  class Visma < Sinatra::Base
    application_name = 'Visma'

    # Send application into development mode
    #
    ENVIRONMENT = Sinatra::Base.development? ? :development : :production

    # Set application root path
    #
    APPLICATION_ROOT = '/visma'

    # Create page title based on site and current page
    #
    def make_title(site, page)
      "#{site} - #{page}"
    end

    # Session Secret Key
    SESSION_SECRET = '~v!i@s#m$a%'.freeze

    # Site Configuration
    configure do
      # Enable static assets
      enable :static

      # Set application.rb folder as root
      set root: File.dirname(__FILE__)

      # Forbidden error workaround until nginx provides a fix
      set :protection, :except => :ip_spoofing
    end

    # Enable sessions for csrf token
    # enable :sessions
    use Rack::Session::Cookie, key: 'rack.session',
                               path: '/',
                               secret: SESSION_SECRET

    # Use Protection
    # use Rack::Protection

    # Cache assets
    use Rack::Cache,
        verbose: true,
        metastore: 'file:/var/cache/rack/meta',
        entitystore: 'file:/var/cache/rack/body'

    # Minify Assets using gzip
    use Rack::Deflater

    # Rewrite trailing '/'
    use Rack::Rewrite do
      r301 %r{^/(.*)/$}, '/$1'
    end

    # Run LiveReload Server
    use Rack::LiveReload, live_reload_port: 35729 if Sinatra::Base.development?

    # For contact message responses
    use Rack::Flash


    # Set views folder
    set :views, File.join(File.dirname(__FILE__), 'views')

    # Set public folder
    if Sinatra::Base.development?
      set :public_folder, File.join(File.dirname(__FILE__), 'src')
    else
      set :public_folder, File.join(File.dirname(__FILE__), 'public')
    end

    # Set configuration data
    env = YAML.load(File.read File.join(
              File.dirname(__FILE__), 'config', 'env.yml'))[ENVIRONMENT]
    config = YAML.load(File.read File.join(
              File.dirname(__FILE__), 'config', 'config.yml'))

    # Load local site data
    locals = {
      env: env,
      title: config[:title],
      description: config[:description],
    }

    # Current application root
    root = Sinatra::Base.development? ? '' : APPLICATION_ROOT

    # Routes
    #
    before do
      @root = root
    end

    get '/' do
      redirect "#{root}/signin" unless session[:authenticated]

      locals[:page_title] = make_title application_name, 'Dashboard'
      locals[:scripts] = ['application/background-changer', 'views/index']
      erb :'home/index', layout: :'layouts/layout', locals: locals
    end

    get "/signin" do
      locals[:page_title] = make_title application_name, 'Sign In'
      locals[:scripts] = ['views/account']
      erb :'account/signin', layout: :'layouts/layout', locals: locals
    end

    post "/signin" do
      session[:authenticated] = true
      redirect "#{root}/"
    end

    get "/signup" do
      locals[:page_title] = make_title application_name, 'Sign Up'
      locals[:scripts] = ['views/account']
      erb :'account/signup', layout: :'layouts/layout', locals: locals
    end

    post "/signup" do
      session[:authenticated] = true
      redirect "#{root}/"
    end

    get "/forgot-password" do
      locals[:page_title] = make_title application_name, 'Forgot Password'
      locals[:scripts] = ['views/account']
      erb :'account/forgot-password', layout: :'layouts/layout', locals: locals
    end

    get "/signout" do
      session[:authenticated] = false
      locals[:scripts] = []
      redirect "#{root}/signin"
    end

    get "/twitter" do
      redirect "#{root}/signin" unless session[:authenticated]

      locals[:page_title] = make_title application_name, 'Twitter'
      locals[:scripts] = ['isotope/isotope', 'moment/moment', 'application/twitter']
      erb :'home/twitter', layout: :'layouts/layout', locals: locals
    end

    # Twitter client configuration
    twitter = Twitter::REST::Client.new do |config|
      config.consumer_key        = "76HBzsuWTxVpfxbbR8c7QbYjL"
      config.consumer_secret     = "WjGDI8SOfoE46In4LrhUbJ1mpCmju5sa5dmLXsOvkAcxHk7aqd"
      config.access_token        = "3008611089-gMc9i7wCq07HczAMmiRgN03mRP0lN4FKKAu8Kn0"
      config.access_token_secret = "yYskIbrl5cmkuGLzmlPyhOvzMHk0IZIM4BV3Lr2zQIUfx"
    end

    get '/api/v1/twitter' do
      content_type :json

      query = params[:q] ? params[:q] : "@visma"
      count = params[:count] ? params[:count].to_i : 10

      tweets = []
      twitter.search(query, result_type: "recent").take(count).collect do |tweet|
        tweets.push tweet.to_h
      end

      tweets.to_json
    end

    # Sitemap
    get '/sitemap' do
      content_type 'text/xml'

      map = XmlSitemap::Map.new("pixevil.com#{APPLICATION_ROOT}", :secure => true) do |m|
        # Templates
        m.add "/templates/volta", priority: 0.8, updated: Date.today, period: :monthly
      end

      map.render
    end

    # 404 Error
    not_found do
      locals[:page_title] = make_title application_name, '404 Not Found'
      locals[:scripts] = []

      status 404
      erb :'error/404', layout: :'layouts/layout', locals: locals
    end

    # 500 Error
    error do
      locals[:page_title] = make_title application_name, '500 Internal Error'
      locals[:scripts] = []

      status 500
      erb :'error/500', layout: :'layouts/layout', locals: locals
    end
  end
end
