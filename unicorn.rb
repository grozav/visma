# Unicorn
#
# Creates a Ruby Sinatra process pid and socket, writes logs and supports
# concurrent web accesses
#
# @author Alex Grozav
#

# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/var/www/visma.pixevil.com"

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "/var/www/visma.pixevil.com/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/logs/unicorn.log"
# stdout_path "/path/to/logs/unicorn.log"
stderr_path "/var/www/visma.pixevil.com/logs/unicorn.log"
stdout_path "/var/www/visma.pixevil.com/logs/unicorn.log"

# Unicorn socket
# listen "/tmp/unicorn.[app name].sock"
listen "/tmp/unicorn.visma.pixevil.com.sock"

# Number of processes
# worker_processes 4
worker_processes 4

# Time-out
timeout 30
