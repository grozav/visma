# Config.ru
#
# Run Sinatra application using Rack
#
# @usage rackup
#
# @author Alex Grozav
#

require './application'

run Application::Visma.new
