# Gulp
#
# Task runner for automated dependency compilation. Automatically transpiles,
# optimizes and minifies assets.
#
# @author Alex Grozav
#
# @usage gulp watch
# @usage gulp build
#

# Gulp dependencies
gulp = require('gulp')
paths = require('./tasks/paths')()
plugins = require('gulp-load-plugins')()
runsequence = require('run-sequence')

# LiveReload port for current application
livereload_port = 35729

# Collect plugins
collect_plugins = [
  'animus'
  # 'slidea'
  # 'revelate'
  # 'smoothscroll'
  # 'imagine'
  # 'image-centered'
]

# JS
gulp.task('coffee', require('./tasks/coffee')(gulp, plugins, paths));
gulp.task('jsmin', require('./tasks/js')(gulp, plugins, paths));

# CSS
gulp.task('stylus', require('./tasks/stylus')(gulp, plugins, paths, require('nib')));
gulp.task('sass', require('./tasks/sass')(gulp, plugins, paths));
gulp.task('scss', require('./tasks/scss')(gulp, plugins, paths));
gulp.task('cssmin', require('./tasks/css')(gulp, plugins, paths));

# Fonts
gulp.task('fonts-copy', require('./tasks/copy').fonts(gulp, plugins, paths));

# Icons
gulp.task('icons-copy', require('./tasks/copy').icons(gulp, plugins, paths));

# Videos
gulp.task('videos-copy', require('./tasks/copy').videos(gulp, plugins, paths));

# Images
gulp.task('imagemin', require('./tasks/images')(gulp, plugins, paths));

# Plugins
# gulp.task('plugins-coffee', require('./tasks/plugins').coffee(gulp, plugins, paths, collect_plugins));
# gulp.task('plugins-sass', require('./tasks/plugins').sass(gulp, plugins, paths, collect_plugins));
# gulp.task('plugins-stylus', require('./tasks/plugins').stylus(gulp, plugins, paths, collect_plugins));

# Testing
gulp.task('test', require('./tasks/test')(gulp, plugins, paths));

# Complete build
gulp.task('build', require('./tasks/build')(gulp, runsequence));
gulp.task('make', require('./tasks/build')(gulp, runsequence));

# Require Watch file
gulp.task('watch', require('./tasks/watch')(gulp, plugins, paths, livereload_port));

# Default task which is run using the 'gulp' command
gulp.task 'default', [
  'watch'
]
