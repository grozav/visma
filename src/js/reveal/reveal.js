
/*
                                             dP
                                             88
88d888b. .d8888b. dP   .dP .d8888b. .d8888b. 88
88'  `88 88ooood8 88   d8' 88ooood8 88'  `88 88
88       88.  ... 88 .88'  88.  ... 88.  .88 88
dP       `88888P' 8888P'   `88888P' `88888P8 dP
oooooooooooooooooooooooooooooooooooooooooooooooooo

A smart and efficient scroll reveal plugin by Alex Grozav
from Pixevil built to make the web a better place.

@plugin  	Reveal
@author 	Alex Grozav
@website  http://pixevil.com
@version 	1.0
@license 	Commercial
 */

(function() {
  (function($, window, document) {
    "use strict";
    $.reveal = function(element, options) {
      var _defaults;
      _defaults = {
        startDelay: 600,
        endDelay: 0,
        edge: [100, 100],
        screen: {
          xs: 0,
          sm: 768,
          md: 992,
          lg: 1200,
          xl: 1560
        },
        loop: false,
        repeat: false,
        preload: true,
        visibility: true,
        perspective: 800,
        perspectiveOrigin: "50% 50%",
        orientation: 'vertical',
        animation: 'opacity 0',
        animate: "TweenLite",
        container: $('body'),
        scroller: $(window)
      };
      this._defaults = _defaults;
      this.settings = $.extend({}, _defaults, options);
      this.element = $(element);
      this.window = this.settings.scroller;
      this.size = {
        element: {},
        window: {}
      };
      this.position = {
        element: {},
        window: {}
      };
      this.data = {};
      this.loaded = false;
      this.debug = this.settings.debug ? this.settings.debug : false;

      /*
      Initialize reveal and gather all the data
       */
      this.initialize = (function(_this) {
        return function() {
          _this.set_element_id();
          _this.set_animator();
          _this.set_orientation(_this.settings.orientation);
          _this.init_animus();
          _this.set_data();
          _this.show_initial();
          _this.set_classes();
          _this.load(function() {
            _this.set_size();
            _this.set_position();
            _this.set_responsive_context();
            _this.bind_resize();
            _this.bind_scroll();
            _this.scroll();
            _this.loaded = true;
            _this.element.triggerHandler('reveal.load', [_this.element]);
            _this.log("Reveal has been initialized.");
          });
        };
      })(this);

      /*
      Load element
       */
      this.load = (function(_this) {
        return function(callback) {
          var loadables, loaded;
          callback.call();
          if (!_this.settings.preload) {
            return;
          }
          if (_this.element.is('img')) {
            loadables = _this.element;
          } else {
            loadables = $('img', _this.element);
          }
          if (loadables.length > 0) {
            loaded = 0;
            loadables.each(function(index, loadable) {
              var image, image_loader, src;
              image = $(loadable);
              if (image.attr('data-imagine-src') != null) {
                src = image.attr('data-imagine-src');
              } else {
                src = image.attr('src');
              }
              image_loader = $("<img>");
              image_loader.attr('src', src);
              image_loader.on('load', function() {
                image.attr('src', src);
                loaded += 1;
                if (loaded === loadables.length) {
                  _this.set_size();
                  _this.set_position();
                  _this.scroll();
                }
              }).on('error', function() {
                _this.error("Image with src=\"" + src + "\" failed to load.");
              });
            });
          }
        };
      })(this);

      /*
      Set element and parent sizes
       */
      this.set_size = (function(_this) {
        return function() {
          _this.size.element[_this.param.size] = _this.element[_this.param.outerSize](true);
          _this.size.window[_this.param.size] = _this.window[_this.param.size]();
          _this.size.viewport = _this.size.window[_this.param.size] - _this.settings.edge[1];
          _this.log("Element sizes have been set.");
        };
      })(this);
      this.setSize = this.set_size;

      /*
      Set element top and bottom positioning on the page
       */
      this.set_position = (function(_this) {
        return function() {
          _this.position.element[_this.param.start] = _this.element.offset()[_this.param.start];
          _this.position.element[_this.param.end] = _this.position.element[_this.param.start] + _this.size.element[_this.param.size];
          _this.position.element.middle = (_this.position.element[_this.param.start] + _this.position.element[_this.param.end]) / 2;
          _this.position.window.max = _this.settings.container[_this.param.size]() - _this.size.window[_this.param.size];
          _this.log("Element positions have been set.");
        };
      })(this);
      this.setPosition = this.set_position;

      /*
      Get individual element animation data
       */
      this.set_data = (function(_this) {
        return function() {
          var animation_stack, current_time_stack, image_size, initial_animation_override, last_time, out_animation, starting_animation, time_stack, timeline;
          _this.data.animation = {};
          if (typeof _this.settings.animation === 'string') {
            _this.settings.animation = {
              initial: _this.settings.animation
            };
          }
          time_stack = [];
          current_time_stack = 0;
          animation_stack = {};
          if (_this.element.is('img')) {
            image_size = _this.get_image_size(_this.element);
            _this.data.width = image_size.width;
            _this.data.height = image_size.height;
          }
          if (_this.element.attr('data-reveal-start-delay') != null) {
            _this.data.start = parseFloat(_this.element.attr('data-reveal-start-delay'), 10);
          } else {
            _this.data.start = _this.settings.startDelay;
          }
          if (_this.element.attr('data-reveal-end-delay') != null) {
            _this.data.end = parseFloat(_this.element.attr('data-reveal-end-delay'), 10);
          } else {
            _this.data.end = _this.settings.endDelay;
          }
          starting_animation = _this.element.attr('data-reveal');
          if (starting_animation == null) {
            starting_animation = _this.element.attr('data-reveal-initial');
          }
          if (starting_animation == null) {
            starting_animation = _this.settings.animation.initial;
          }

          /*
          This sets the initial state of our animated object
          The entering animation will be set as css and will
          transition to the default state
           */
          animation_stack[_this.data.start] = starting_animation;
          time_stack[current_time_stack++] = _this.data.start;

          /*
          Set animation in override to set a different beginning state
          other than the default one
           */
          if (_this.element.attr('data-reveal-in') != null) {
            initial_animation_override = _this.animus.get(_this.element.attr('data-reveal-in'));
          } else if (_this.settings.animation["in"] != null) {
            initial_animation_override = _this.animus.get(_this.settings.animation["in"]);
          } else {
            initial_animation_override = false;
          }
          timeline = _this.element.data();
          if (timeline != null) {
            $.each(timeline, function(key, value) {
              var at_time, time;
              time = void 0;
              if ((time = key.match(/revealAt([0-9]+)/)) !== null) {
                at_time = parseInt(time[1], 10);
                animation_stack[at_time] = value;
                time_stack[current_time_stack++] = at_time;
              }
            });
          }
          $.each(_this.settings.animation, function(index, value) {
            if (!/[0-9]+/.test(index)) {
              return;
            }
            animation_stack[index] = value;
            time_stack[current_time_stack++] = index;
          });
          last_time = 0;

          /*
          The time stack is needed to maintain the order of
          the object animations since JSON objects aren't ordered
           */
          time_stack.sort();
          $.each(time_stack, function(key, time) {
            _this.data.animation[time_stack[key]] = _this.animus.get(animation_stack[time_stack[key]]);
            if (time > last_time) {
              last_time = time;
            }
          });
          if (_this.element.attr('data-reveal-out') != null) {
            out_animation = _this.element.attr('data-reveal-out');
          } else if (_this.settings.animation.out) {
            out_animation = _this.settings.animation.out;
          } else {
            out_animation = starting_animation;
          }
          if (out_animation !== '') {
            _this.data.animation[-1] = _this.animus.get(out_animation);
          }

          /*
          Set reset state by getting all the animation variables
          and setting them to the default values
           */
          if ($.type(_this.data.animation[_this.data.start].state) !== 'string') {
            _this.data.animation.initial = {
              timeline: null,
              duration: 0,
              state: _this.animus.reset(_this.data.animation[_this.data.start].state, _this.data.animation)
            };
            if (!('opacity' in _this.data.animation.initial.state)) {
              _this.data.animation.initial.state.opacity = 1;
            }
            _this.data.animation[_this.data.start].state = _this.animus.reset(initial_animation_override.state, _this.data.animation);
            if ('ease' in _this.data.animation.initial.state) {
              _this.data.animation[_this.data.start].state.ease = _this.data.animation.initial.state.ease;
            }
          } else {
            _this.data.animation.initial = {
              timeline: null,
              duration: 0,
              state: {
                opacity: 0
              }
            };
          }
          if (((_this.element.attr('data-reveal-loop') != null) && _this.element.attr('data-reveal-loop') === 'true') || _this.settings.loop) {
            _this.data.loop = parseInt(last_time) + _this.data.animation[last_time].duration * 1000;
            _this.data.loop_diff = _this.data.start + _this.data.animation[_this.data.start].duration * 1000;
          } else {
            _this.data.loop = false;
          }
          if ((_this.element.attr('data-reveal-repeat') != null) || _this.settings.repeat) {
            _this.data.repeat = true;
          } else {
            _this.data.repeat = false;
          }
          if (_this.element.attr('data-reveal-threshold') != null) {
            _this.data.threshold = parseInt(_this.element.attr('data-reveal-threshold'), 10) / 100;
          } else if (_this.settings.threshold) {
            _this.data.threshold = _this.settings.threshold / 100;
          }
          if (_this.settings.callback) {
            _this.data.callback = _this.settings.callback;
          }
        };
      })(this);

      /*
      Add dynamic element classes and
       */
      this.set_classes = (function(_this) {
        return function() {
          var reveal_style, style;
          if ($('#reveal-style').length === 0) {
            reveal_style = ".reveal-parent { perspective: " + _this.settings.perspective + "px; -moz-perspective: " + _this.settings.perspective + "px; -webkit-perspective: " + _this.settings.perspective + "px; perspective-origin: " + _this.settings.perspectiveOrigin + "; -moz-perspective-origin: " + _this.settings.perspectiveOrigin + "; -webkit-perspective-origin: " + _this.settings.perspectiveOrigin + "; backface-visibility: hidden; -moz-backface-visibility: hidden; -webkit-backface-visibility: hidden; } .reveal-animated{ transform-style: preserve-3d; -moz-transform-style: preserve-3d; -webkit-transform-style: preserve-3d; transform-origin: 50% 50%; -ms-transform-origin: 50% 50%; -moz-transform-origin: 50% 50%; -webkit-transform-origin: 50% 50%; }";
            if (_this.settings.visibility) {
              reveal_style += "\n.reveal-hidden{ visibility: hidden; }";
              reveal_style += "\n.reveal-visible{ visibility: visible; }";
            }
            style = document.createElement('style');
            style.id = 'reveal-style';
            style.type = 'text/css';
            style.innerHTML = reveal_style;
            $('head')[0].appendChild(style);
          }
          _this.element.addClass('reveal-hidden reveal-animated');
          _this.element.parent().addClass('reveal-parent');
        };
      })(this);
      this.init_animus = (function(_this) {
        return function() {
          var override;
          override = {
            duration: _this.settings.animation.duration,
            easing: _this.settings.animation.easing
          };
          _this.animus = new $.animus(override);
        };
      })(this);

      /*
      Initialize
       */
      this.show_initial = (function(_this) {
        return function() {
          _this.animate.set(_this.element, _this.data.animation.initial.state);
          _this.element.addClass('reveal-visible');
          _this.element.removeClass('reveal-hidden');
          if ('callback' in _this.data) {
            _this.data.callback.call(_this, _this.element, 'initial');
          }
          _this.element.triggerHandler('reveal.show');
        };
      })(this);

      /*
      Runs animation
       */
      this.show = (function(_this) {
        return function(in_loop) {
          var loop_timeout;
          if (_this.data == null) {
            return;
          }
          $.each(_this.data.animation, function(index, animation) {
            var timeout;
            if (index === "initial" || index === '-1') {
              return;
            }
            _this.log("Running transition[" + index + "].");
            _this.log(_this.data.animation[index]);
            timeout = index;
            _this.data.animation[index].timeline = setTimeout(function() {
              if (_this.data.callback != null) {
                _this.data.callback.call(_this, _this.element, index);
              }
              if (typeof _this.data.animation[index].state === 'string') {
                _this.animate_preset(_this.data.animation[index]);
              } else {
                _this.animate.to(_this.element, _this.data.animation[index].duration, _this.data.animation[index].state);
              }
            }, timeout);
          });
          if (_this.data.loop) {
            if (in_loop) {
              loop_timeout = _this.data.loop - _this.data.loop_diff;
            } else {
              loop_timeout = _this.data.loop;
            }
            _this.data.loop_timeout = setTimeout(function() {
              _this.show(true);
            }, loop_timeout);
          }
        };
      })(this);

      /*
      Runs animation
       */
      this.hide = (function(_this) {
        return function() {
          if (_this.data == null) {
            return;
          }
          setTimeout(function() {
            _this.animate.to(_this.element, _this.data.animation[-1].duration, _this.data.animation[-1].state);
          }, _this.data.end);
          _this.element.addClass('reveal-hidden');
          _this.element.removeClass('reveal-visible');
          _this.show_initial();
          if ('callback' in _this.data) {
            _this.data.callback.call(_this, _this.element, 'out');
          }
          _this.element.triggerHandler('reveal.hide');
        };
      })(this);

      /*
      Run animus animation preset
       */
      this.animate_preset = (function(_this) {
        return function(data) {
          var timeout;
          _this.log("Animating preset " + data + ".");
          timeout = 0;
          $.each($.animus.presets[data.state], function(index, animation) {
            var duration;
            duration = data.duration * animation[1];
            setTimeout(function() {
              _this.animate.to(_this.element, duration, $.animus.presets[data.state][index][0]);
            }, timeout * 1000);
            timeout += duration;
          });
        };
      })(this);
      this.in_viewport = function() {
        var bottom, max, min, ref, scroll, top;
        top = this.position.element[this.param.start];
        bottom = this.position.element[this.param.end];
        scroll = this.position.window[this.param.start];
        ref = scroll < this.settings.edge[0] ? [0, scroll + this.size.viewport] : scroll === this.position.window.max ? [scroll + this.settings.edge[0], scroll + this.size.viewport + this.settings.edge[1]] : [scroll + this.settings.edge[0], scroll + this.size.viewport], min = ref[0], max = ref[1];
        return (top >= min && bottom <= max) || (top <= min && (min < bottom && bottom < max)) || ((min < top && top < max) && bottom >= max) || (top <= min && bottom >= max);
      };

      /*
      Scroll animation handler
       */
      this.in_view_eariler = false;
      this.scroll = (function(_this) {
        return function() {
          _this.position.window[_this.param.start] = _this.window[_this.param.scroll]();
          _this.position.window[_this.param.end] = _this.position.window[_this.param.start] + _this.size.window[_this.param.size];
          if (_this.in_viewport()) {
            if (!_this.in_view_eariler) {
              if (!_this.data.repeat) {
                _this.window.off('scroll', _this.scroll);
              }
              _this.in_view_eariler = true;
              _this.show();
              _this.element.triggerHandler('reveal.visible', [_this.in_view]);
            }
          } else {
            if (_this.data.repeat && _this.in_view_eariler) {
              _this.in_view_eariler = false;
              _this.hide();
              _this.element.triggerHandler('reveal.hidden', [_this.in_view]);
            }
          }
        };
      })(this);

      /*
      Bind the window scroll event to fade content on scroll down
       */
      this.bind_scroll = (function(_this) {
        return function() {
          _this.window.on('scroll', _this.scroll);
        };
      })(this);

      /*
      Binds the window resize event to cache current window
      width and height and to set the layout up
       */
      this.bind_resize = (function(_this) {
        return function() {
          _this.window.resize(function() {
            _this.set_size();
            _this.set_position();
            _this.set_responsive_context();
            _this.scroll();
          });
        };
      })(this);

      /*
      Set the element id if it doesn't have one or
      get the existing one
       */
      this.set_element_id = (function(_this) {
        return function() {
          if (_this.element.attr('id') != null) {
            _this.id = _this.element.attr('id');
          } else {
            _this.id = _this.get_random_id('reveal');
            _this.element.attr('id', _this.id);
          }
          _this.log("Element id has been set to " + _this.id + ".");
        };
      })(this);

      /*
      Get a random id by concatenating input string
      with a random number
       */
      this.get_random_id = function(string) {
        return string + '-' + Math.floor((Math.random() * 100000) + 1);
      };

      /*
      Setup Animation Platform
       */
      this.set_animator = (function(_this) {
        return function() {
          _this.animate = window[_this.settings.animate];
          _this.log("Animating using the " + _this.settings.animate + " platform.");
        };
      })(this);

      /*
      Setup Animation Platform
       */
      this.set_animator = (function(_this) {
        return function() {
          _this.animate = window[_this.settings.animate];
          _this.log("Animating using the " + _this.settings.animate + " platform.");
        };
      })(this);

      /*
      Sets parameters based on orientation settings
       */
      this.set_orientation = (function(_this) {
        return function(orientation) {
          _this.param = {};
          if (orientation === 'vertical') {
            _this.param.size = 'height';
            _this.param.outerSize = 'outerHeight';
            _this.param.middle = 'vmiddle';
            _this.param.start = 'top';
            _this.param.end = 'bottom';
            _this.param.axis = 'y';
            _this.param.scroll = 'scrollTop';
            _this.param.half = 'halfHeight';
          } else {
            _this.param.size = 'width';
            _this.param.outerSize = 'outerWidth';
            _this.param.middle = 'hmiddle';
            _this.param.start = 'left';
            _this.param.end = 'right';
            _this.param.axis = 'x';
            _this.param.scroll = 'scrollLeft';
            _this.param.half = 'halfWidth';
          }
        };
      })(this);
      this.setOrientation = this.set_orientation;

      /*
      Set current responsive range parameter as xs, sm, md, lg or xl
       */
      this.set_responsive_context = (function(_this) {
        return function() {
          if (_this.position.window.width >= _this.settings.screen.xl) {
            _this.current_responsive_size = 'xl';
          } else if (_this.position.window.width >= _this.settings.screen.lg) {
            _this.current_responsive_size = 'lg';
          } else if (_this.position.window.width >= _this.settings.screen.md) {
            _this.current_responsive_size = 'md';
          } else if (_this.position.window.width >= _this.settings.screen.sm) {
            _this.current_responsive_size = 'sm';
          } else {
            _this.current_responsive_size = 'xs';
          }
          _this.log("Responsive context is " + _this.current_responsive_size + ".");
        };
      })(this);

      /*
      Get the size of an image element
       */
      this.get_image_size = (function(_this) {
        return function(image) {
          var size;
          size = {};
          size.width = image[0].naturalWidth != null ? image[0].naturalWidth : image[0].width != null ? image[0].width : image.width != null ? image.width() : 'auto';
          size.height = image[0].naturalHeight != null ? image[0].naturalHeight : image[0].height != null ? image[0].height : image.height != null ? image.height() : 'auto';
          return size;
        };
      })(this);

      /*
      Logger snippet within Reveal
       */
      this.log = (function(_this) {
        return function(item) {
          if (!_this.debug) {
            return;
          }
          if (typeof item === 'object') {
            console.log("[Reveal " + _this.id + "]", item);
          } else {
            console.log("[Reveal " + _this.id + "] " + item);
          }
        };
      })(this);

      /*
      Error logger snippet within Reveal
       */
      this.error = (function(_this) {
        return function(item) {
          if (typeof item === 'object') {
            console.error("[Reveal " + _this.id + "]", item);
          } else {
            console.error("[Reveal " + _this.id + "] " + item);
          }
        };
      })(this);
      this.initialize();
    };
    return $.fn.reveal = function(opts) {
      return this.each(function(index, element) {
        if (!$.data(element, "reveal")) {
          return $.data(element, "reveal", new $.reveal(element, opts));
        }
      });
    };
  })(window.jQuery, window, document);

}).call(this);
