(function() {
  $(document).ready((function(_this) {
    return function() {
      var applications, background_changer;
      background_changer = $('.background-changer');
      applications = $('.application');
      return applications.each(function(index, application) {
        var application_theme;
        application = $(application);
        application_theme = application.attr('data-application');
        application.on('mouseenter', function() {
          background_changer.addClass("-" + application_theme);
        });
      });
    };
  })(this));

}).call(this);

//# sourceMappingURL=../../maps/application/background.js.map
