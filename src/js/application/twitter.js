(function() {
  (function($, window, document) {
    "use strict";
    $.twitterFeed = function(element, options) {
      this.element = $(element);
      this.url = this.element.attr('data-url');
      this.initialize = function() {
        return $.ajax({
          url: this.url,
          type: "GET",
          data: {
            q: "#visma",
            count: 12
          },
          success: (function(_this) {
            return function(response) {
              console.log(response);
              _this.build(response);
              _this.masonry();
              _this.reveal();
            };
          })(this),
          error: function(jqXHR, statusText, errorThrown) {
            console.error(statusText);
          }
        });
      };
      this.escape = function(text) {
        return $('<div/>').text(text).html();
      };
      this.parse = function(text, entities) {
        $.each(entities.urls, (function(_this) {
          return function(i, entry) {
            text = text.replace(entry.url, "<a class='tweet-entity' href='" + _this.escape(entry.url) + "'>" + _this.escape(entry.url) + "</a>");
          };
        })(this));
        $.each(entities.hashtags, (function(_this) {
          return function(i, entry) {
            text = text.replace("#" + entry.text, "<a class='tweet-entity' href='http://twitter.com/search?q=" + escape("#" + entry.text) + "'>#" + _this.escape(entry.text) + "</a>");
          };
        })(this));
        $.each(entities.user_mentions, (function(_this) {
          return function(i, entry) {
            text = text.replace("@" + entry.screen_name, "<a class='tweet-entity' title='" + _this.escape(entry.name) + "' href='http://twitter.com/" + _this.escape(entry.screen_name) + "'>@" + _this.escape(entry.screen_name) + "</a>");
          };
        })(this));
        return text;
      };
      this.build = function(tweets) {
        var j, len, name, profile, text, time_ago, tweet, tweets_html, username;
        tweets_html = "";
        for (j = 0, len = tweets.length; j < len; j++) {
          tweet = tweets[j];
          text = this.parse(tweet.text, tweet.entities);
          name = tweet.user.name;
          username = tweet.user.screen_name;
          time_ago = moment(tweet.created_at).fromNow();
          profile = tweet.user.profile_image_url;
          tweets_html += "<div class='tweet'> <div class='tweet-time'> " + time_ago + " </div> <div class='tweet-user'> <a href='https://twitter.com/" + username + "'> <img class='tweet-user-avatar' src='" + profile + "' alt='" + username + " Twitter'/> <strong class='tweet-user-name'>" + name + "</strong> </a> <a class='tweet-user-username' href='https://twitter.com/" + username + "'>@" + username + "</a> </div> <p class='tweet-text'> " + text + " </p> </div>";
        }
        this.element.append(tweets_html);
        this.tweets = $('.tweet');
      };
      this.masonry = function() {
        return this.element.isotope({
          itemSelector: '.tweet'
        });
      };
      this.reveal = function() {
        return this.tweets.each((function(_this) {
          return function(index, tweet) {
            $(tweet).reveal({
              animation: "opacity 0, y -50",
              startDelay: index * 50,
              edge: [0, 0]
            });
          };
        })(this));
      };
      this.initialize();
    };
    $.fn.twitterFeed = function(opts) {
      return this.each(function(index, element) {
        if (!$.data(element, "twitterFeed")) {
          return $.data(element, "twitterFeed", new $.twitterFeed(element, opts));
        }
      });
    };
  })(window.jQuery, window, document);

  $(document).ready(function() {
    $('.twitter-feed').twitterFeed();
  });

}).call(this);

//# sourceMappingURL=../../maps/application/twitter.js.map
