(function() {
  (function($, window, document) {
    "use strict";
    $.backgroundChanger = function(element, options) {

      /*
      Application items
       */
      var applications, background_changer, current_theme, themes;
      background_changer = $(element);
      applications = $('.application');

      /*
      Initialize Background Changer
       */
      themes = "";
      current_theme = "";
      TweenLite.set(background_changer, {
        opacity: 0

        /*
        Add bindings for each application item
         */
      });
      applications.each(function(index, application) {
        var application_theme;
        application = $(application);
        application_theme = application.attr('data-application');
        themes += "-" + application_theme + " ";
        application.on('mouseenter', function() {
          current_theme = "-" + application_theme;
          background_changer.removeClass(themes);
          background_changer.addClass(current_theme);
          TweenLite.to(background_changer, 0.33, {
            opacity: 1
          });
        });
        application.on('mouseleave', function() {
          var theme_to_remove;
          theme_to_remove = current_theme;
          TweenLite.to(background_changer, 0.33, {
            opacity: 0,
            onComplete: function() {
              background_changer.removeClass(theme_to_remove);
            }
          });
        });
      });
    };
    $.fn.backgroundChanger = function(opts) {
      return this.each(function(index, element) {
        if (!$.data(element, "backgroundChanger")) {
          return $.data(element, "backgroundChanger", new $.backgroundChanger(element, opts));
        }
      });
    };
  })(window.jQuery, window, document);

  $(document).ready(function() {
    $('.background-changer').backgroundChanger();
  });

}).call(this);

//# sourceMappingURL=../../maps/application/background-changer.js.map
