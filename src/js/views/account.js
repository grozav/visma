(function() {
  $(document).ready(function() {
    var form;
    form = $('.form');
    return form.reveal({
      animation: "opacity 0, duration 1000",
      startDelay: 0,
      edge: [0, 0]
    });
  });

}).call(this);

//# sourceMappingURL=../../maps/views/account.js.map
