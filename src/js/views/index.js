(function() {
  $(document).ready(function() {
    var applications;
    applications = $('.application');
    applications.slice(0, 4).each(function(index, item) {
      $(item).reveal({
        animation: "opacity 0, y -100, rotationX 90",
        startDelay: index * 300,
        edge: [0, 0]
      });
    });
    applications.slice(4, 8).each(function(index, item) {
      $(item).reveal({
        animation: "opacity 0, y 100, rotationX -90",
        startDelay: index * 300,
        edge: [0, 0]
      });
    });
  });

}).call(this);

//# sourceMappingURL=../../maps/views/index.js.map
