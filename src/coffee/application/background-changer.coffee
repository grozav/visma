# Background Changer
#
# Background changer script for changing the color based on the application
# being hovered. 
#
# @author Alex Grozav
#

(($, window, document) ->
  "use strict"

  # @backgroundChanger
  $.backgroundChanger = (element, options) ->
    ###
    Application items
    ###
    background_changer = $(element)
    applications = $('.application')

    ###
    Initialize Background Changer
    ###
    themes = ""
    current_theme = ""
    TweenLite.set background_changer, opacity: 0

    ###
    Add bindings for each application item
    ###
    applications.each (index, application) ->
      application = $(application)
      application_theme = application.attr 'data-application'

      # Add to theme classes list
      themes += "-#{application_theme} "

      application.on 'mouseenter', ->
        current_theme = "-#{application_theme}"

        background_changer.removeClass themes
        background_changer.addClass current_theme

        TweenLite.to background_changer, 0.33, opacity: 1
        return

      application.on 'mouseleave', ->
        theme_to_remove = current_theme
        TweenLite.to background_changer, 0.33,
          opacity: 0
          onComplete: ->
            background_changer.removeClass theme_to_remove
            return
        return
      return
    return

  # Lightweight plugin wrapper that prevents multiple instantiations.
  #
  $.fn.backgroundChanger = (opts) ->
    @each (index, element) ->
      unless $.data element, "backgroundChanger"
        $.data element, "backgroundChanger", new $.backgroundChanger element, opts
  return
) window.jQuery, window, document

$(document).ready ->
  $('.background-changer').backgroundChanger()
  return
