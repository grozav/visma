# Twitter
#
# Obtains tweets via ajax and builds tweet elements.
#
# @uses https://api.twitter.com/1.1/search/tweets.json
#
# @author Alex Grozav
#

(($, window, document) ->
  "use strict"

  # @twitterFeed
  $.twitterFeed = (element, options) ->
    @element = $(element)
    @url = @element.attr 'data-url'

    @initialize = ->
      $.ajax
        url: @url
        type: "GET"
        data:
          q: "#visma"
          count: 12
        success: (response) =>
          console.log response

          @build response
          @masonry()
          @reveal()
          return
        error: (jqXHR, statusText, errorThrown) ->
          console.error statusText
          return


    @escape = (text) ->
      return $('<div/>').text(text).html()


    @parse = (text, entities) ->
      $.each entities.urls, (i, entry) =>
        text = text.replace entry.url, "<a class='tweet-entity' href='" + @escape(entry.url) + "'>" + @escape(entry.url) + "</a>"
        return

      $.each entities.hashtags, (i, entry) =>
        text = text.replace "##{entry.text}", "<a class='tweet-entity' href='http://twitter.com/search?q=" + escape("#" + entry.text) + "'>#" + @escape(entry.text) + "</a>"
        return

      $.each entities.user_mentions, (i,entry) =>
        text = text.replace "@#{entry.screen_name}", "<a class='tweet-entity' title='" + @escape(entry.name) + "' href='http://twitter.com/" + @escape(entry.screen_name) + "'>@" + @escape(entry.screen_name) + "</a>"
        return

      return text


    @build = (tweets) ->
      tweets_html = ""

      for tweet in tweets
        text = @parse tweet.text, tweet.entities
        name = tweet.user.name
        username = tweet.user.screen_name
        time_ago = moment(tweet.created_at).fromNow()
        profile = tweet.user.profile_image_url

        tweets_html += "
          <div class='tweet'>
            <div class='tweet-time'>
              #{time_ago}
            </div>
            <div class='tweet-user'>
              <a href='https://twitter.com/#{username}'>
                <img class='tweet-user-avatar' src='#{profile}' alt='#{username} Twitter'/>
                <strong class='tweet-user-name'>#{name}</strong>
              </a>
              <a class='tweet-user-username' href='https://twitter.com/#{username}'>@#{username}</a>
            </div>
            <p class='tweet-text'>
              #{text}
            </p>
          </div>
        "

      @element.append tweets_html

      @tweets = $('.tweet')
      return


    @masonry = ->
      @element.isotope
        itemSelector: '.tweet'

    @reveal = ->
      @tweets.each (index, tweet) =>
        $(tweet).reveal
          animation: "opacity 0, y -50"
          startDelay: index * 50
          edge: [0, 0]
        return

    @initialize()
    return

  # Lightweight plugin wrapper that prevents multiple instantiations.
  #
  $.fn.twitterFeed = (opts) ->
    @each (index, element) ->
      unless $.data element, "twitterFeed"
        $.data element, "twitterFeed", new $.twitterFeed element, opts
  return
) window.jQuery, window, document

$(document).ready ->
  $('.twitter-feed').twitterFeed()
  return
