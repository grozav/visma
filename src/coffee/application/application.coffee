# Application
#
# Base script included in all existing pages
#
# @author Alex Grozav
#

$(document).ready ->
  # Navbar
  #
  $('.navbar').navbar()

  # Morph dropdown
  #
  $('.navbar').morphDropdown()

  return
