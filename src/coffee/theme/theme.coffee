# Theme
#
# Provides application colors
#
# @author Alex Grozav
#

do ->
  colors =
    primary: '#e81943'
    secondary: '#1c0a0e'

    aqua: '#7fdfff'
    blue: '#0074d9'
    navy: '#001f3f'
    teal: '#39cccc'
    green: '#2ecc40'
    olive: '#3d9970'
    lime: '#01ff70'
    yellow: '#ffdc00'
    orange: '#ff851b'
    red: '#ff4136'
    fuchsia: '#f012be'
    purple: '#b10dc9'
    maroon: '#85144b'

    white: '#ffffff'
    silver: '#f2f2f2'
    darksilver: '#d1d1d1'
    gray: '#aaaaaa'
    lightgray: '#cccccc'
    concrete: '#1d1d1e'
    asphalt: '#3b3b3b'
    black: '#111111'

    facebook: '#3b5998'
    twitter: '#1da1f2'
    'google-plus': '#dd4b39'
    dribbble: '#ea4c89'
    behance: '#1769ff'
    flickr: '#ff0084'
    linkedin: '#0077b5'
    youtube: '#cd201f'
    pinterest: '#bd081c'
    github: '#333333'
    tumblr: '#35465c'
    twitch: '#6441a5'
    envato: '#82b541'
