# Base Theme
#
# Provides basic color conversions
#
# @author Alex Grozav
#
do ->
  hexToRgba = (hex, alpha) ->
    result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    alpha = 1 unless alpha?
    rgb =
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    "rgba(#{rgb.r}, #{rgb.g}, #{rgb.b}, #{alpha})"

  hexShade = (color, percent) ->
    f = parseInt(color.slice(1), 16)
    t = if percent < 0 then 0 else 255
    p = if percent < 0 then percent * -1 else percent
    R = f >> 16
    G = f >> 8 & 0x00FF
    B = f & 0x0000FF
    '#' + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + Math.round((t - B) * p) + B).toString(16).slice(1)

  rgbShade = (color, percent) ->
    f = color.split(',')
    t = if percent < 0 then 0 else 255
    p = if percent < 0 then percent * -1 else percent
    R = parseInt(f[0].slice(4))
    G = parseInt(f[1])
    B = parseInt(f[2])
    'rgb(' + Math.round((t - R) * p) + R + ',' + Math.round((t - G) * p) + G + ',' + Math.round((t - B) * p) + B + ')'

  hexBlend = (c0, c1, p) ->
    f = parseInt(c0.slice(1), 16)
    t = parseInt(c1.slice(1), 16)
    R1 = f >> 16
    G1 = f >> 8 & 0x00FF
    B1 = f & 0x0000FF
    R2 = t >> 16
    G2 = t >> 8 & 0x00FF
    B2 = t & 0x0000FF
    '#' + (0x1000000 + (Math.round((R2 - R1) * p) + R1) * 0x10000 + (Math.round((G2 - G1) * p) + G1) * 0x100 + Math.round((B2 - B1) * p) + B1).toString(16).slice(1)
