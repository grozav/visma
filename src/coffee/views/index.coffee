# Index
#
# Provides bindings for the index view
#
# @author Alex Grozav
#
$(document).ready ->
  applications = $('.application')

  applications.slice(0, 4).each (index, item) ->
    $(item).reveal
      animation: "opacity 0, y -100, rotationX 90"
      startDelay: index * 300
      edge: [0, 0]
    return

  applications.slice(4, 8).each (index, item) ->
    $(item).reveal
      animation: "opacity 0, y 100, rotationX -90"
      startDelay: index * 300
      edge: [0, 0]
    return

  return
