# Account
#
# Provides bindings for the account view
#
# @author Alex Grozav
#
$(document).ready ->
  form = $('.form')

  form.reveal
    animation: "opacity 0, duration 1000"
    startDelay: 0
    edge: [0, 0]
