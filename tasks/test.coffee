# Test
#
# Run test suite using Jasmine
#
module.exports = (gulp, plugins, paths) =>
  return =>
    gulp.src('spec/*.js')
      .pipe(plugins.jasmine())
