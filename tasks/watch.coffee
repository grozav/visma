# Watch
#
# Watch for file changes
#
module.exports = (gulp, plugins, paths, livereload_port, make) =>
  return =>
    plugins.livereload.listen
      port: livereload_port

    gulp.watch paths.assets_src + paths.coffee + '**/*.coffee', [ 'coffee' ]
    gulp.watch paths.assets_src + paths.js + '**/*.js', ['jsmin']

    gulp.watch paths.assets_src + paths.stylus + '**/*.styl', [ 'stylus' ]
    gulp.watch paths.assets_src + paths.sass + '**/*.scss', [ 'scss' ]
    gulp.watch paths.assets_src + paths.sass + '**/*.sass', [ 'sass' ]
    gulp.watch paths.assets_src + paths.css + '**/*.css', ['cssmin']

    gulp.watch paths.assets_src + paths.img + '**/*', [ 'imagemin' ]

    gulp.watch paths.assets_src + paths.fonts + '**/*', [ 'fonts-copy' ]
    gulp.watch paths.assets_src + paths.video + '**/*', [ 'videos-copy' ]
    gulp.watch paths.assets_src + paths.icon + '**/*', [ 'icons-copy' ]

    gulp.watch paths.plugins + '**/*.styl', [ 'plugins-stylus' ]
    gulp.watch paths.plugins + '**/*.sass', [ 'plugins-sass' ]
    gulp.watch paths.plugins + '**/*.coffee', [ 'plugins-coffee' ]

    gulp.watch paths.views + '**/*.{op,md,html,erb}', =>
      plugins.livereload.reload()
      return
    return
