# Gemfile
#
# Ruby application dependencies, installed using Bundler
#
# @author Alex Grozav
#
source 'https://rubygems.org'
ruby '2.3.0'

# Rack
gem 'rack-rewrite'
gem 'rack-cache'
gem 'rack-protection'
gem 'rack-flash3'

# Application
gem 'sinatra'
gem 'sinatra-contrib'
gem 'sinatra-jsonp'
gem 'mail'
gem 'shotgun'
gem 'unicorn'
gem 'kramdown'
gem 'mustache'

# Models
gem 'activerecord'
gem 'sinatra-activerecord'

# Analytics
gem 'browser'
gem 'geoip'

# Sitemap
gem 'xml-sitemap'

# Twitter API
gem 'twitter'

# Development
group :development do
  gem 'rspec'
  gem 'rack-test'
  gem 'rubyzip'
  gem 'watir-webdriver'

  gem 'rack-livereload'
  gem 'guard-livereload', require: false
end
